# Material Questions

# - where did the speaker say she worked?
# - what color did one of the speakers dye her hair?

# - parts of an agent?
# - types of agents?
# - what is big data?
# - what is meta examinations?
# - how useful is a map of scale 1:1?
# - why don't simulations match reality?

# - aims of AI?

# - please write the program to generate random data?
# - normally how much time do you need to sort something? Give big O notation
# - what is a stochastic environment?
# - name two things which are always present in reinforcement learning?
# - what makes reinforcement learning systems data efficient?

# - what is the difference between meta learning / learning?
# ========================================
# Notes

# - https://gitlab.com/gitcourses/iiitmk-ai-2019/merge_requests/419/diffs
# - https://gitlab.com/gitcourses/iiitmk-ai-2019/issues/24
# - https://gitlab.com/gitcourses/iiitmk-ai-2019/issues/26
# - https://www.csee.umbc.edu/courses/471/papers/turing.pdf
#
#
# ========================================
# 1. Copy an assignment file from this folder to the `people/<your name>` folder
# 2. Edit the `people/<your name>/<assignment number>.py` file and write your solution in it
# 3. Create a merge request to submit
# 4. Ensure that
#       - tests pass for your solution in the merge request
#       - you have setup your gitlab fork to auto-mirror the class
#
#
# =========================================
# same name as "world"
def road_cross(look) -> bool:
    ...
